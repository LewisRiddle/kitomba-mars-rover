# Mars Rover Simulation

## Usage

Follow system prompts as follows:
1. Enter the upper-right coordinates in the format of 5 5
2. Enter the amount of rovers you want   
3. Enter rover position in the format of 1 2 N
4. Enter rover movements in the format of MMLRLMM
5. Repeat steps 3 and 4 for each rover
6. Resulting coordinates will be printed

## Design

A Controller object deals with user I/O, using a Plateau object to store Rover locations, and a list of Rovers to control their internal logic in interactions with the Plateau (e.g. preventing collisions).

## Assumptions Made

* The plateau is a perfect rectangle and contains no obstructions
* The amount of rovers and their size is unknown
* Rovers cannot turn in diagonal directions
* Rovers cannot move in a different direction to that which they are facing
* Rovers will not be placed outside the plateau
