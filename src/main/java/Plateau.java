package main.java;

public class Plateau {

    private final boolean[][] grid;

    private final int maxX;
    private final int maxY;

    /**
     * Board used by main.java.Controller to remember where rovers are.
     * @param maxX Far right of plateau
     * @param maxY Top of plateau
     */
    public Plateau(int maxX, int maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
        this.grid = new boolean[maxX+1][maxY+1];
    }

    /**
     * Constructor used to generate a copy.
     * @param maxX Far right of plateau
     * @param maxY Top of plateau
     * @param grid Grid of rover locations
     */
    public Plateau(int maxX, int maxY, boolean[][] grid) {
        this.maxX = maxX;
        this.maxY = maxY;
        this.grid = grid;
    }

    public int getMaxX() {
        return this.maxX;
    }

    public int getMaxY() {
        return this.maxY;
    }

    /**
     * Checks if a rover can be moved to this location
     * @param x coordinate
     * @param y coordinate
     * @return Move position valid?
     */
    public boolean validMovePosition(int x, int y) {
        return !this.grid[x][y];
    }

    public void removeRover(int x, int y) {
        this.grid[x][y] = false;
    }

    public void addRover(int x, int y) {
        this.grid[x][y] = true;
    }

    public Plateau copy() {
        boolean[][] newGrid = new boolean[grid.length][];
        for(int i=0; i<grid.length; i++) {
            newGrid[i] = grid[i].clone();
        }
        return new Plateau(maxX, maxY, newGrid);
    }
}
