package main.java;

public class Rover {

    private int posX;
    private int posY;

    /**
     * Direction enum for direction rover points at
     */
    public enum Direction{
        NORTH('N'), EAST('E'), SOUTH('S'), WEST('W');

        /**
         * Convert to character value for printing
         * @return Char
         */
        public char toChar() {
            return toChar;
        }

        private final char toChar;

        Direction(char toChar) {
            this.toChar = toChar;
        }
    }

    private Direction dir;

    /**
     * main.java.Rover objects handled by the controller
     * @param x Current x position
     * @param y Current y position
     * @param d Current direction
     */
    public Rover(int x, int y, Direction d) {
        this.posX = x;
        this.posY = y;
        this.dir = d;
    }

    /**
     * Tests the result of a movement without actually moving
     * @return Final position after movement, with x as 0 index and y as 1 index
     */
    public int[] testMove() {
        int[] positions = new int[]{this.posX, this.posY};
        switch(dir) {
            case NORTH -> positions[1]++;
            case EAST -> positions[0]++;
            case SOUTH -> positions[1]--;
            case WEST -> positions[0]--;
        }
        return positions;
    }

    /**
     * Move rover
     */
    public void move() {
        switch(dir) {
            case NORTH -> posY++;
            case EAST -> posX++;
            case SOUTH -> posY--;
            case WEST -> posX--;
        }
    }

    /**
     * Turn rover left
     */
    public void turnLeft() {
        switch(dir) {
            case NORTH -> this.dir = Direction.WEST;
            case EAST -> this.dir = Direction.NORTH;
            case SOUTH -> this.dir = Direction.EAST;
            case WEST -> this.dir = Direction.SOUTH;
        }
    }

    /**
     * Turn rover right
     */
    public void turnRight() {
        switch(dir) {
            case NORTH -> this.dir = Direction.EAST;
            case EAST -> this.dir = Direction.SOUTH;
            case SOUTH -> this.dir = Direction.WEST;
            case WEST -> this.dir = Direction.NORTH;
        }
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction d) {
        dir = d;
    }

    public Rover copy() {
        return new Rover(posX, posY, dir);
    }
}
