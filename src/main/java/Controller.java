package main.java;

import java.util.ArrayList;
import java.util.Scanner;

public class Controller {
    private Plateau plat;
    private final ArrayList<Rover> rovers = new ArrayList<>();

    /**
     * Controller to deal with user I/O
     */
    public Controller() {
    }

    public void beginScan() {
        Scanner sc = new Scanner(System.in);
        this.upperRightCoordinates(sc);

        System.out.println("Final coordinates and cardinality for the rovers will be listed below.\n");
        int currentRover = 1;
        for(Rover r : rovers) {
            System.out.printf("Rover #%d: %d %d %c\n", currentRover, r.getPosX(), r.getPosY(), r.getDir().toChar());
            currentRover++;
        }
        sc.close();
    }

    /**
     * Defines upper right coordinates to create plateau
     * @param sc Scanner
     */
    public void upperRightCoordinates(Scanner sc) {
        int maxX = -1;
        int maxY = -1;
        // Repeat until valid coordinates
        while(maxX < 0 || maxY < 0) {
            System.out.println("Enter the upper right coordinates of the plateau (separated by a space):");
            Scanner scLine = new Scanner(sc.nextLine());
            if(scLine.hasNextInt()) {
                maxX = scLine.nextInt();
                if(maxX < 0) {
                    System.out.println("You must enter a positive x-coordinate.");
                }
            }
            else {
                System.out.println("Please enter an valid x-coordinate.");
            }
            if(scLine.hasNextInt()) {
                maxY = scLine.nextInt();
                if(maxY < 0) {
                    System.out.println("You must enter a positive y-coordinate.");
                }
            }
            else {
                System.out.println("Please enter an valid y-coordinate.");
            }
            scLine.close();
        }
        this.plat = new Plateau(maxX, maxY);
        this.defineRovers(sc);
    }

    /**
     * Defines number of rovers
     * @param sc Scanner
     */
    public void defineRovers(Scanner sc) {
        int roverAmount = -1;
        while(roverAmount < 1) {
            System.out.println("Enter how many rovers you want:");
            if(!sc.hasNextInt()) {
                System.out.println("Please enter a valid number.");
                sc.nextLine();
            }
            else {
                roverAmount = sc.nextInt();
                if(roverAmount < 1) {
                    System.out.println("There must be at least one rover.");
                }
                if(roverAmount > (plat.getMaxX()+1) * (plat.getMaxY()+1)) {
                    System.out.println("You cannot fit this many rovers.");
                    roverAmount = -1;
                }
            }
        }
        sc.nextLine();
        for(int i=0; i<roverAmount; i++) {
            this.positionRover(sc, i);
        }
    }

    /**
     * Defines positions of rovers
     * @param sc Scanner
     * @param i Index of rover
     */
    public void positionRover(Scanner sc, int i) {
        int x = -1;
        int y = -1;
        Rover.Direction d = null;
        while(x < 0 || y < 0 || d == null) {
            System.out.printf("Enter the position for rover #%d and its cardinal direction in uppercase, all separated by spaces (e.g. 3 2 N):\n", i+1);
            Scanner scLine = new Scanner(sc.nextLine());
            if(scLine.hasNextInt()) {
                x = scLine.nextInt();
                if(x < 0) {
                    System.out.println("You must enter a positive x-coordinate.");
                }
            }
            else {
                System.out.println("Please enter an valid x-coordinate.");
            }
            if(scLine.hasNextInt()) {
                y = scLine.nextInt();
                if(y < 0) {
                    System.out.println("You must enter a positive y-coordinate.");
                }
            }
            else {
                System.out.println("Please enter an valid y-coordinate.");
            }
            // Must prevent negative coordinates
            if(x < 0 || y < 0) {
                continue;
            }
            if(this.plat.validMovePosition(x, y)) {
                if(scLine.hasNext()) {
                    String strDir = scLine.next();
                    switch(strDir){
                        case "N" -> d = Rover.Direction.NORTH;
                        case "E" -> d = Rover.Direction.EAST;
                        case "S" -> d = Rover.Direction.SOUTH;
                        case "W" -> d = Rover.Direction.WEST;
                        default -> System.out.println("Please enter a valid direction.");
                    }
                }
                else {
                    System.out.println("You must enter a direction.");
                }
            }
            else {
                System.out.println("There is already a rover in this position.");
            }
            scLine.close();
        }
        this.rovers.add(new Rover(x, y, d));
        this.moveRover(sc, i);
    }

    /**
     * Moves rovers and ensures movements are valid
     * @param sc Scanner
     * @param i Index of rover
     */
    public void moveRover(Scanner sc, int i) {
        Rover r = this.rovers.get(i); // Current rover
        // Copy information in case of invalid movement
        Rover roverCopy = r.copy();
        Rover.Direction dirCopy = r.getDir();
        Plateau platCopy = this.plat.copy();

        boolean validMovement = false;
        while(!validMovement) {
            System.out.printf("Enter instructions for rover #%d, using only L, R and M in uppercase:\n", i+1);
            // Going with a true until proven otherwise approach
            validMovement = true;
            String line = sc.nextLine();
            for(int j=0; j<line.length(); j++) {
                switch(line.charAt(j)) {
                    case 'L' -> r.turnLeft();
                    case 'R' -> r.turnRight();
                    case 'M' -> {
                        // Test future position to check if valid
                        int[] positions = r.testMove();
                        if(0 <= positions[0] && positions[0] <= this.plat.getMaxX()
                                && 0 <= positions[1] && positions[1] <= this.plat.getMaxY()) {
                            if(this.plat.validMovePosition(positions[0], positions[1])) {
                                // Must move rover between removing and adding from the grid
                                this.plat.removeRover(r.getPosX(), r.getPosY());
                                r.move();
                                this.plat.addRover(r.getPosX(), r.getPosY());
                            }
                            else {
                                System.out.println("There is already a rover in this position.");
                                validMovement = false;
                            }
                        }
                        else {
                            System.out.println("You cannot move off the plateau.");
                            validMovement = false;
                        }
                    }
                }
                // Immediately stop if invalid movement
                if(!validMovement) {
                    // Revert rover and plateau to their initial state
                    r = roverCopy;
                    r.setDir(dirCopy);
                    this.plat = platCopy;
                    break;
                }
            }
        }

    }
}
