package test.java;

import main.java.MarsRover;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {
    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayOutputStream testOut;

    /**
     * Sets up output stream for reference in assertions
     */
    @BeforeEach
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    /**
     * Provides an input to System.In
     * @param data String to input
     */
    private void provideInput(String data) {
        ByteArrayInputStream testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    /**
     * Obtains output of System.out
     * @return Output
     */
    private String getOutput() {
        return testOut.toString();
    }

    /**
     * Restores system I/O for next test
     */
    @AfterEach
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void testAcceptableInput1() {
        provideInput("5 5\n2\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Rover #1: 1 3 N"));
        assertTrue(getOutput().contains("Rover #2: 5 1 E"));
    }

    @Test
    public void testAcceptableInput2() {
        provideInput("1 1\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Rover #1: 1 0 S"));
    }

    @Test
    public void testAcceptableInput3() {
        provideInput("10 10\n3\n0 0 E\nMMMMMRLLMMM\n10 10 S\nMM\n5 5 S\nLRRRLRLR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Rover #1: 5 3 N"));
        assertTrue(getOutput().contains("Rover #2: 10 8 S"));
        assertTrue(getOutput().contains("Rover #3: 5 5 N"));
    }

    @Test
    public void testNegativeXCoord() {
        provideInput("-1 1\n1 1\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("You must enter a positive x-coordinate."));
    }

    @Test
    public void testInvalidXCoord() {
        provideInput("\n1 1\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Please enter an valid x-coordinate."));
    }

    @Test
    public void testNegativeYCoord() {
        provideInput("1 -1\n1 1\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("You must enter a positive y-coordinate."));
    }

    @Test
    public void testInvalidYCoord() {
        provideInput("1\n1 1\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Please enter an valid y-coordinate."));
    }

    @Test
    public void testInvalidRovers1() {
        provideInput("1 1\nabc\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Please enter a valid number."));
    }

    @Test
    public void testInvalidRovers2() {
        provideInput("1 1\n0\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("There must be at least one rover."));
    }

    @Test
    public void testInvalidRovers3() {
        provideInput("1 1\n9\n1\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("You cannot fit this many rovers."));
    }

    @Test
    public void testInvalidRoverPosition1() {
        provideInput("1 1\n1\n-2 -2 S\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("You must enter a positive x-coordinate."));
        assertTrue(getOutput().contains("You must enter a positive y-coordinate."));
    }

    @Test
    public void testInvalidRoverPosition2() {
        provideInput("1 1\n1\nS\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Please enter an valid x-coordinate."));
        assertTrue(getOutput().contains("Please enter an valid y-coordinate."));
    }

    @Test
    public void testInvalidRoverPosition3() {
        provideInput("1 1\n1\n0 0\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("You must enter a direction."));
    }

    @Test
    public void testInvalidRoverPosition4() {
        provideInput("1 1\n1\n0 0 w\n0 0 W\nLRLRRRMR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("Please enter a valid direction."));
    }

    @Test
    public void testInvalidRoverPosition5() {
        provideInput("1 1\n2\n0 0 N\nM\n0 1 E\n1 1 E\nR");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("There is already a rover in this position."));
    }

    @Test
    public void testInvalidRoverMovement1() {
        provideInput("1 1\n2\n0 0 N\nM\n0 0 N\nM\nRM");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("There is already a rover in this position."));
    }

    @Test
    public void testInvalidRoverMovement2() {
        provideInput("1 1\n1\n0 0 W\nM\nL");
        MarsRover.main(new String[0]);
        assertTrue(getOutput().contains("You cannot move off the plateau."));
    }
}